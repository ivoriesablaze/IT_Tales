import clients
import player_class

class Ticket:
    def __init__(self, ticket_number, issue, owner, complexity, steps_until_fail, reward, is_resolved=False, failed=False):
        self.ticket_number=ticket_number        
        self.issue = issue
        self.owner = owner
        self.complexity = complexity
        self.steps_until_fail = steps_until_fail
        self.reward = reward
        self.is_resolved = is_resolved
        self.failed = failed

    def __repr__ (self):
        pass

    def resolve_ticket (self):
        if self.failed == True:
            print("Ticket already failed.")
            return self.is_resolved
        else:
            self.is_resolved = True
            return self.is_resolved
    
    def change_steps (self, is_decrement):
        if is_decrement == False:
            self.steps_until_fail += 1
        else:
            self.steps_until_fail -= 1
        return self.steps_until_fail

    def has_failed(self):
        if self.is_resolved == True:
            print("Ticket already resolved.")
            return self.failed
        else:
            self.failed=True
            return self.failed