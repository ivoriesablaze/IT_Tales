import random
from player_class import Player
from client_class import Client
import clients
import ITTalesASCII
from game_over_ascii import GameOver
def job_response(answer, counter):
    if answer == "Y":
        print("\nGreat! Lets get started!\n")
    elif answer == "N":
        # Answering no will prompt one of 5 responses that passive aggressively guilt the player into accepting the position
        # This calls the negative_job_offer function
        negative_job_offer(counter)
    else:
        print("\nYou can't even give a coherent response! You'll fit right in!\n")

def negative_job_offer(counter):
    job_offer = ""
    if counter == 0:
        # There's no ME in team!... wait...
        job_offer = input("Come on, be a team player! (y or n) ")
        job_offer = job_offer.upper()
        counter += 1
        job_response(job_offer, counter)
    elif counter == 1:
        # You didn't even look at the fine print, did you! Dumbass.
        job_offer = input("But you already signed the contract... (y or n) ")
        job_offer = job_offer.upper()
        counter += 1
        job_response(job_offer, counter)
    elif counter == 2:
        # We're a "fun" workplace!
        job_offer = input("But we have a welcome party all ready to go! (y or n) ")
        job_offer = job_offer.upper()
        counter += 1
        job_response(job_offer, counter)
    elif counter == 3:
        # The over dramatic guilt-trip response
        job_offer = input("Please? Otherwise you're dooming our organization to bankruptcy! (y or n) ")
        job_offer = job_offer.upper()
        counter += 1
        job_response(job_offer, counter)
    else:
        # Yeah, what were you gonna do? Go to Geek Squad? We own your ass!
        job_offer = input("That's cute that you think you have a choice. (y or n) ")
        job_offer = job_offer.upper()
        job_response(job_offer, counter)

def get_player_info():
    print(tech)

def main_menu():
    # The player chooses an option
    print("Main Menu\n1. Next Battle... er... Ticket\n2. {tech}'s Stats\n3. Store\n4. Quit\n5. Add Money (testing)\n6. Add Review (testing)\n7. Add Sanity (testing)".format(tech=tech.name))
    player_choice = input("How would you like to proceed, {tech}? ".format(tech=tech.name))
    if player_choice == "1":
        print("\nThere are currently no tickets in queue. Your sanity is safe... for now.\n")
        main_menu()
    elif player_choice == "2":
        print()
        # Grabs information from the player object.
        print(tech)
        print()
        main_menu()
    elif player_choice == "3":
        print("\nBeat it kid, you're broke.\n")
        main_menu()
    elif player_choice == "4":
        player_quit = input("Are you sure you want to quit? We can make every Friday pizza day if you don't leave! (y or n) ")
        player_quit = player_quit.upper()
        if player_quit == "Y":
            print("\nDon't bother asking for a reference letter.\n")
        elif player_quit == "N":
            print("Great! By the way, I just found out we don't have the budget for a pizza party. Sorry!")
            main_menu()
        else:
            print("I'll take your incoherence to mean that you're staying. Great! Also, we can't actually do pizza. Here's a coupon.")
            main_menu()
    # The below options are for testing
    elif player_choice == "5":
        money_to_add = input("How much richer do you want to be? ")
        tech.add_money(int(money_to_add))
        main_menu()
    elif player_choice == "6":
        new_rating = input("What review to give yourself?")
        new_rating = int(new_rating)
        tech.change_rating(new_rating)
        main_menu()
    elif player_choice == "7":
        sanity_change = int(input("How much has your sanity changed today?: "))
        tech.change_sanity(sanity_change)
        if tech.sanity <= 0:
            print("We have a new patient for the psychaitrist to see at the mental institution.")
            print("")
            GameOver.game_over()
        else:
            print(f'Sanity is {tech.sanity}')
            main_menu()
    # Just a little DOOM easter egg.
    elif player_choice == 'iddqd':
        print("INVULNERABILITY!!!")
        print("What does that do for you in this game? Absolutely nothing.")
        main_menu()
    else:
        print("\nOutstanding! Seriously, though, quit slacking.\n")
        main_menu()

#def menu_choice(player_input):
#   pass
# The counter is to keep track of the amount of times the player has responded to the offer with No.
# print(clients.patient_client_dict['Anthony'][1])
counter = 0
ITTalesASCII.Ascii.print_logo()
print("Welcome to IT_TALES!")
#If you've worked in IT, you'll get why we call them whiny
print("Let's get started on the fulfilling occupation that is tending to whiny... er... distressed users at the Solo Corp!")
print()
name = input("We'll start with your name: ")
tech = Player(name)
print()
print("Welcome {name}!".format(name=tech.name))
job_offer = input("Are you ready to accept our job offer and start helping the company keep its well oiled machinery running? (y or n) ")
job_offer = job_offer.upper()
job_response(job_offer, counter)

# print(tech)
main_menu()

print("Thank you for taking a look at the beginning stages of my game.")
print("I will continue working to get this made into a full game hopefully by November 2023.")