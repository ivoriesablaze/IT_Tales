class Client:
    def __init__(self, name, patience_level, savvy, ego, rating_given = False):
        self.name = name
        self.patience_level = patience_level
        self.savvy = savvy
        self.ego = ego
        self.rating_given = rating_given
        self.moves = []
        self.rating = 0

    def __repr__(self):
        pass

    def change_patience(self, patience_change):
        self.patience_level.append(self.patience_level - patience_change)
        return self.patience_level

    def give_rating(self):
        self.rating_given = True
        return self.rating_given
    # The player_rating method is used to determine the rating given to the player based on the final outcome of the battle
    # Maybe put in another factor such as starting patience_level
    def player_rating(self, patience_left, steps_left_before_fix):
        self.give_rating()
        if patience_left <= 100:
            if steps_left_before_fix <= 0:
                self.rating = 1
                return self.rating
            elif steps_left_before_fix > 0 and steps_left_before_fix <= 15:
                self.rating = 2
                return self.rating
            elif steps_left_before_fix > 10 and steps_left_before_fix <= 30:
                self.rating = 3
                return self.rating
            elif steps_left_before_fix > 25 and steps_left_before_fix <= 45:
                self.rating = 4
                return self.rating
            else:
                self.rating = 5
                return self.rating
        elif patience_left <= 200:
            if steps_left_before_fix <= 0:
                self.rating = 1
                return self.rating
            elif steps_left_before_fix > 0 and steps_left_before_fix <= 10:
                self.rating = 2
                return self.rating
            elif steps_left_before_fix > 10 and steps_left_before_fix <= 25:
                self.rating = 3
                return self.rating
            elif steps_left_before_fix > 25 and steps_left_before_fix <= 40:
                self.rating = 4
                return self.rating
            else:
                self.rating = 5
                return self.rating
        elif patience_left <= 300:
            if steps_left_before_fix <= 0:
                self.rating = 1
                return self.rating
            elif steps_left_before_fix > 0 and steps_left_before_fix <= 5:
                self.rating = 2
                return self.rating
            elif steps_left_before_fix > 5 and steps_left_before_fix <= 20:
                self.rating = 3
                return self.rating
            elif steps_left_before_fix > 20 and steps_left_before_fix <= 35:
                self.rating = 4
                return self.rating
            else:
                self.rating = 5
                return self.rating
        elif patience_left <= 400:
            if steps_left_before_fix >= 0 and steps_left_before_fix <= 10:
                self.rating = 2
                return self.rating
            elif steps_left_before_fix > 10 and steps_left_before_fix <= 25:
                self.rating = 3
                return self.rating
            elif steps_left_before_fix > 25 and steps_left_before_fix <= 40:
                self.rating = 4
                return self.rating
            else:
                self.rating = 5
                return self.rating