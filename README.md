# Python Terminal Game
## IT_TALES
### Overview

This terminal-based Python game will "recreate" the day to day experience of an IT technician (spoiler alert, it's nothing like it).
The player received tickets coming into the company's IT department and you have a set of tools at your disposal. These problems range from the mundane, normal, everyday IT issue (I can't connect to my printer) to the complete outrageous you can't believe these people exist stupid (my cupholder won't eject anymore, used dishwasher to clean laptop, microwaved phone to charge wirelessly, etc.).
The tools you use work in a Pokemon style battle against the issue. While some issues are going to be resolved by obvious methods, there will be a dice roll to determine if and what complication may get in the way of the resolution (printer not working - set default printer to resolve... default printer resets) in which case using the option again can still fix the issue if the dice roll is favorable.

If the issue can't be resolved in so many turns, you lose sanity (what would normally be classed as hit points) and resort to having to replace their device. If the issue is resolved, the user will give you a feedback score out of 5 stars. Each star will earn so much money which can be used for special items (for example, tacos will allow you to skip dice rolls on attempted resolutions and can fix the problem in one step if it is "super effective") or various caffeinated beverages that will restore sanity. Each star will also roll the dice between specific numbers (depending on number of stars) to give you XP to level up to new tiers.

The game ends either when all sanity is lost and you are taken to a mental institution or you achieve the coveted Tier 3 position and defeat the boss - Karen.