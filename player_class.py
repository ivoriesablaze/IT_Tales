import ticket_class
from game_over_ascii import GameOver

class Player:

    def __init__(self, name, level=1, sanity=100, money=0, tickets_solved=0, reviews=0, ticket_fail_history = None, ticket_resovled_history = None):
        self.name = name
        self.level = level
        self.sanity = sanity
        self.money = money
        self.tickets_solved = tickets_solved
        self.reviews = reviews
        self.items = []
        self.player_ratings = []
        self.player_rating = 0
        self.ticket_fail_history = ticket_fail_history
        self.ticket_resolved_history = ticket_resovled_history
    
    def __repr__(self):
        ticket_desc = ""
        review_desc = ""

        if self.tickets_solved == 1:
            ticket_desc = "ticket"
        else:
            ticket_desc = "tickets"

        if self.reviews == 1:
            review_desc = "time"
        else:
            review_desc = "times"

        description = """Player name: {name}
        Player level: {level}
        Player sanity: {sanity}
        Player cash: ${money}
        Tickets resolved: {tickets_solved}
        Player reviews: {reviews}
        Average rating: {player_rating}.""".format(name=self.name,level=self.level,sanity=self.sanity,money=self.money,tickets_solved=self.tickets_solved,reviews=self.reviews,player_rating=self.player_rating)
        return description

    # change_money() method for successful tickets - need to incorporate ticket_class with this
    def change_money(self, money_amount):
        self.money += money_amount
        return self.money
    
    # change_sanity() method can be 
    def change_sanity(self, sanity_amount):
        self.sanity += sanity_amount
        return self.sanity

    def level_up(self):
        self.level += 1
        return self.level

    def resolved(self):
        self.tickets_solved += 1
        return self.tickets_solved

    def new_review(self):
        self.reviews += 1
        return self.reviews
    # The client_rating should be coming from the client_class
    def change_rating(self, client_rating):
        total = 0
        if client_rating <= 5 and client_rating >= 1:
            self.new_review()
            self.player_ratings.append(client_rating)
            for individual_ratings in self.player_ratings:
                print(individual_ratings)
                print(self.player_ratings)
                total += individual_ratings
        else:
            print("\n***Please ensure the rating is between 1 and 5***\n")
            return self.player_rating
        print(total)
        self.player_rating = total/self.reviews
        return self.player_rating

   # def sanity_check(self):
   #     if self.sanity <= 0:
   #         print("We have a new patient for the psychaitrist to see at the mental institution.")
   #         print("")
   #         GameOver.game_over()
   #     else:
   #         print(f'Sanity is {self.sanity}')