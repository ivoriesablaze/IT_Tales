class Client_List:
    # This will be a random list of clients that submitted tickets to the player
    # Karen is the "boss" (final) client with the highest stats and small chance at a one-hit "I want to see the manager" kill move.
    # Dictionary stats template - {Name, [[patience_level], savvy, ego, moves]}

    # The patient_client_dict dictionary lists clients (enemies) that you basically can't lose against no matter the issue.
    # They will almost always give favorable reviews regardless of the player's attitude just as long as the ticket is resolved.
    # Bad attitudes may take away a star depending on how long it took to resolve.
    patient_client_dict = {"Anthony": [[500], 10, 1, []], "Sara": [[500], 8, 1, []], "Zak": [[450], 5, 3, []], "Suzie": [[500], 8, 2, []]}

    # The reasonable_client_dict dictionary lists clients that may have slightly less patience than the previous dictionary, but due to a higher savvy, will be more understandable 
    # for tickets, though may or may not get more irritable as the battle goes on.
    # Because they are savvy, some moves may actually help the player. For example, a move may be countered by "client already tried troubleshooting step" which would
    # decrease the amount of moves tried by one, though in some cases, it could also decrease client patience, though highly unlikely in this group.
    reasonable_client_dict = {"Layla": [[350], 10, 4, []], "Angie": [[375], 7, 3, []], "Daniel": [[325], 6, 4, []], "Jude": [[350], 8, 3,[]]}

    # The arrogant_client_dict dictionary lists clients that have lower patience, decent savvy, and insanely high egos.
    # With these clients, the "client already tried troubleshooting step" move will not decrease a move and will drastically cut into their patience.
    arrogant_client_dict = {"Jessica": [[250], 6, 8, []], "Jimmy": [[225], 7, 9, []], "Roxanne": [[215], 5, 10, []], "Leroy": [[200], 5, 10, []]}

    # The cocky_client_dict lists clients that have extremely low patience, almost no savvy, and have very high opinions of themselves.
    # These are the "Don't you know who I am!?!?!?" kind of people... you know, the ones you want to say to them "No, I don't know nor do I care who you are."
    # "Please kindly fuck off and get back in line." Of course, saying this is pretty much an automatic ticket failed and unfavorable review... 
    # though can sometimes be very satisfying and has a 50/50 shot of restoring all sanity.
    cocky_client_dict = {"Mack": [[150], 2, 10, []], "Mary": [[125], 3, 9, []], "Jack": [[175], 1, 10, []], "Diane": [[145], 2, 8, []]}

    # The clueless_client_dict lists clients that have higher patience, very low savvy, and they know it (low ego). While these are not as easy as
    # the patient_client_dict, they are still hard to anger and very understanding.
    clueless_client_dict = {"Stacy": [[475], 1, 2, []], "Jessie": [[450], 2, 1, []], "Rosanna": [[500], 2, 2, []], "Tommy": [[490], 1, 1, []]}

    # The arrogant_patient_client_dict lists clients that have high patience, medium to low savvy, and very high egos. These clients may insult you and complain,
    # but are actually patient and will give you a decent rating as long as the issue is fixed. It's a kind of troll class made to frustrate the player but rewards them
    # for sucking it up and getting the job done.
    arrogrant_patient_client_dict = {"Amy": [[450], 3, 8, []], "Maggie": [[400], 4, 9, []], "Julio": [[425], 3, 7, []], "Danny": [[375], 5, 9, []]}

    # The know_it_all_client_dict lists clients that medium patience, extermely high savvy, and high egos. Basically, they have good tech skills and they know it.
    # Why they're asking you to fix their issue instead of fixing it themselves (which they should be able to) is a mystery that must be ignored.
    know_it_all_client_dict = {"Annie": [[300], 8, 6], "Johnny": [[275], 9, 7, []], "Eileen": [[325], 7, 6, []], "Bobby": [[300], 9, 6, []]}