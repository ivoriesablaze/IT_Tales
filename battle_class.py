import player_class
import client_class
import clients
import ticket_class

class Battle:

    def __init__(self, turn, player_action, client_action, turn_result, item_used):
        self.turn = turn
        self.player_action = player_action
        self.client_action = client_action
        self.turn_result = turn_result
        self.item_used = item_used

    def __repr__(self):
        pass
